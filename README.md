# brightness-osd

Tiny script to manipulate brightness. Useful for WM like i3

# Dependencies

```
sudo apt install python3-pip libnotify-bin
sudo pip3 install confight
```

# Setup
You need to setup where brightnessctl is located you can also specify if you want to use sudo
```
x11tete11x@jarvis:~$ cat .config/brightness-osd/config.toml
brightnessctl_path="/usr/bin/brightnessctl"
sudo=true
```

# Usage
```
usage: brightness-osd.py [-h] [-d D] [-s S]
                         [--brightnessctl-path BRIGHTNESSCTL_PATH] [--sudo]

optional arguments:
  -h, --help            show this help message and exit
  -d D                  Device
  -s S                  Increase/Decrease brightness by the specified
                        percentage
  --brightnessctl-path BRIGHTNESSCTL_PATH
                        Set brightnessctl path
  --sudo                Use sudo to execute brightnessctl
```

# Setting up sudo with no password to use brightnessctl
```
x11tete11x@jarvis:~$ cat /etc/sudoers.d/12-brightness
x11tete11x ALL=(ALL) NOPASSWD: /usr/bin/brightnessctl
```

# Example i3 config
```
x11tete11x@jarvis:~$ cat .config/i3/config | grep bright
# Sreen brightness controls
bindsym XF86MonBrightnessUp exec /home/x11tete11x/GIT/brightness-osd/brightness-osd.py -s 10%+ # increase screen brightness
bindsym XF86MonBrightnessDown exec /home/x11tete11x/GIT/brightness-osd/brightness-osd.py -s 10%- # decrease screen brightness

```

![alt text](example.png "Running")
