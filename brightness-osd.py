#!/usr/bin/env python3
import argparse
import confight
import re
import subprocess

def percent_type(arg_value, pat=re.compile('^[1-9][0-9]?%[+,-]|100%[+,-]')):
    if not pat.match(arg_value):
        raise argparse.ArgumentTypeError
    return arg_value

parser = argparse.ArgumentParser()
parser.add_argument('-d', help="Device")
parser.add_argument('-s',type=percent_type, help="Increase/Decrease brightness by the specified percentage")
parser.add_argument('--brightnessctl-path', help="Set brightnessctl path")
parser.add_argument('--sudo',help="Use sudo to execute brightnessctl", action='store_true')
args = parser.parse_args()

brightnessctl_array=[]
max_brightness=""

def setup_args():
    global args
    conf=confight.load_user_app('brightness-osd')
    if args.d == None:
        args.d=conf.get('device')
    if args.brightnessctl_path == None:
        args.brightnessctl_path=conf.get('brightnessctl_path')
        if args.brightnessctl_path == None:
            args.brightnessctl_path=subprocess.check_output(['which','brightnessctl']).decode(encoding='utf-8').rstrip()
    if not args.sudo:
        args.sudo=conf.get('sudo')

def setup_brightnessctl_invocation():
    global brightnessctl_array
    global args
    if args.sudo:
        brightnessctl_array=['sudo']
    brightnessctl_array.append(args.brightnessctl_path)
    if args.d != None:
        brightnessctl_array.append('-d')
        brightnessctl_array.append(args.d)

def max_brightness():
    global max_brightness
    brightnessctl_arg=brightnessctl_array.copy()
    brightnessctl_arg.append('m')
    max_brightness=int(subprocess.check_output(brightnessctl_arg).decode(encoding='utf-8'))

def initial_setup():
    setup_args()
    setup_brightnessctl_invocation()
    max_brightness()

def get_brightness():
    brightnessctl_arg=brightnessctl_array.copy()
    brightnessctl_arg.append('g')
    return(int(subprocess.check_output(brightnessctl_arg).decode(encoding='utf-8')))

def get_percent(arg):
    return(int(float(arg)*100/float(max_brightness)))

def notify_send():
    subprocess.run(['notify-send','-i','display-brightness','"Brightness"','-h','int:value:'+str(get_percent(get_brightness())),'-h','string:synchronous:brightness'])

def set_brightness(arg):
    brightnessctl_arg=brightnessctl_array.copy()
    brightnessctl_arg.append('s')
    brightnessctl_arg.append(arg)
    subprocess.run(brightnessctl_arg)
    notify_send()

if __name__ == '__main__':
    initial_setup()
    set_brightness(args.s)
